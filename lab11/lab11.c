/*Atryino Dolon
 * AADQ67
 */
#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>
#include<string.h>
#define MAX_LINE 250
int isDate(char* string);
int isPhoneNumber(char* string);
int isName(char* string);
int isEmail(char* word);
int main(int argc, char **argv){
	if(argc != 2){
		printf("Invalid command line\n");
		return 0;
	}
	FILE* in;
	if((in = fopen(*(argv+1), "r"))==NULL){
		printf("Invalid input file\n");
		return 0;
	}
	int count=0;
	char* token, check, *prevtoken=NULL, *name;
	char* line = malloc(MAX_LINE*sizeof(char));
	printf("\n");
	while(!feof(in)){
		fgets(line, MAX_LINE, in);
		if(line[strlen(line)-1] == '\n')
			line[strlen(line)-1] = '\0';
		if((token = strtok(line, ",!? "))!=NULL){
			do{
				if(isDate(token))
					printf("Found a date: %s\n", token);
				if(isPhoneNumber(token))
					printf("Found a phone number: %s\n", token);
				if(isName(&token[1]))
					if(prevtoken!=NULL)
						if(isName(&prevtoken[0]))
							printf("Name found: %s %s\n", token, prevtoken);
				if(isEmail(token))
					printf("Found an email: %s\n", token);
				if(feof(in))
					break;
				prevtoken = token;
				count++;
			}while((token = strtok(NULL, ",!? "))!=NULL);
			
		}
	}
	printf("There were %d words in the file\n\n", count);
	free(line);
	fclose(in);
	return 0;
}
int isDate(char* string){//check for propper date format with '/' at position 2 and 5
	char check;
	int day, month;
	check = string[2];
	if(check=='/'){
		check=string[5];
		if(check=='/'){
			month = atoi(string);
			day = atoi(string+3);
			if(month>0&&month<13)//check that the month and day are valid 
				if(day>0&&day<32)
					return 1;
		}
	}
	return 0;
}
int isPhoneNumber(char* string){
	char check;
	if(strlen(string)==14){
		check = string[0];//check that the number is 14 chars with the correct values at the correct positions
		if(check == '('){
			check=string[4];
			if(check==')'){
				check=string[5];
				if(check=='-'){
					check=string[9];
					if(check=='-')
						return 1;
				}
			}
		}
	}
	return 0;
}
int isName(char* string){
	if(isupper(string[0]))
			return 1;
	return 0;
}
int isEmail(char* word){
	int length = strlen(word), i=0, at = 0;
	char* str = word;
	while(i<length){
		if(str[i]=='@')//check for both @ and .com
			at = 1;
		if(str[i]=='.')
			break;
		i++;
	}
	if(at==1){
		if(strcmp(&str[i], ".com")==0)
			return 1;
	}
	return 0;
}
